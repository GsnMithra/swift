import Foundation

var age = 19
let name = "Mithra"

var result = "Mithra's age is: \(age)"

var paragraph =
"""
    This is a paragraph! O'la
"""

var fruits: [String] = []
fruits.append ("Orange")

if fruits.contains ("Orange") {
    print ("Oranges in fruits.")
} else {
    print ("Oranges not in fruits")
}

var map: [Int: Int] = [:]

map[10] = 100;
map[100] = 200;

print (map[10] == nil);

var email: String? = "mithragsn9@gmail.com";
var phone: Int64 = 6300800476;

print (email ?? phone)

if let userEmailId = email {
    print (userEmailId)
}

var interestingNumbers = [
    "Prime": [2, 3, 5, 7, 11, 13],
    "Fibonacci": [1, 1, 2, 3, 5, 8],
    "Square": [1, 4, 9, 16, 25],
]

func getDictionaryMax (_ numbers: [String: [Int]]) -> (Int, String) {
    var maxValue = Int.min
    
    for (t, n) in interestingNumbers {
        maxValue = max (maxValue, n.max ().unsafelyUnwrapped);
    }
    
    return ((maxValue == Int.min ? -1 : maxValue), "Mithra")
}

var (maxValue, fullName) = getDictionaryMax (interestingNumbers)

print (fullName)


func fibonacci (_ n: Int64) -> Int64 {
    var cache = [Int64: Int64] ()
    
    func fibUtil (_ n: Int64) -> Int64 {
        if let value = cache[n] {
            return value
        }
        
        cache[n] = (n <= 1) ? n : fibUtil (n - 1) + fibUtil (n - 2)
        return cache[n].unsafelyUnwrapped;
    }
    
    return fibUtil (n)
}

print (fibonacci (10))
